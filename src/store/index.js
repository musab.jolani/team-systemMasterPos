import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

const dataState = createPersistedState({
  key: "TokenMusab",
  storage: sessionStorage,
});

export default new Vuex.Store({
  state: { JWToken: "" },
  modules: {},
  mutations: {
    setToken: (state, token) => (state.JWToken = token),
  },
  plugins: [dataState],
});
